#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <conio.h>
#include <fstream>

using namespace std;

int GetCpuUsage()
{
    static ULARGE_INTEGER TimeIdle, TimeKernel, TimeUser;
    FILETIME Idle, Kernel, User;
    ULARGE_INTEGER uIdle, uKernel, uUser;
    GetSystemTimes(&Idle, &Kernel, &User);
    memcpy(&uIdle, &Idle, sizeof(FILETIME));
    memcpy(&uKernel, &Kernel, sizeof(FILETIME));
    memcpy(&uUser, &User, sizeof(FILETIME));
    long long t;
    t = (((((uKernel.QuadPart - TimeKernel.QuadPart) + (uUser.QuadPart - TimeUser.QuadPart)) -
        (uIdle.QuadPart - TimeIdle.QuadPart)) * (100)) / ((uKernel.QuadPart -
            TimeKernel.QuadPart) + (uUser.QuadPart - TimeUser.QuadPart)));
    TimeIdle.QuadPart = uIdle.QuadPart;
    TimeUser.QuadPart = uUser.QuadPart;
    TimeKernel.QuadPart = uKernel.QuadPart;
    return(static_cast<int>(t));
}

struct Converter{
    static const double tb2b;
    static const double gb2b;
    static const float mb2b;
    static const float kb2b;
};
const double Converter::tb2b = pow(2, 40);
const double Converter::gb2b = pow(2, 30);
const float Converter::mb2b = pow(2, 20);
const float Converter::kb2b = pow(2, 10);

class Ram
{
public:
    Ram() {
        statex.dwLength = sizeof(statex);
    }

    void updateStatus(){
        GlobalMemoryStatusEx(&statex);
    }

    float freeRamCapacity() {
        return static_cast<float>(statex.ullAvailPhys) / Converter::gb2b;
    }

    float totalRamCapacity() {
        return static_cast<float>(statex.ullTotalPhys) / Converter::gb2b;
    }

    float usingRamCapacity() {  
        return totalRamCapacity() - freeRamCapacity();
    }

    float usingRamInPercentage() {
        return statex.dwMemoryLoad;
    }

private:
    MEMORYSTATUSEX statex = {};
};

int main() {
    Ram ram;

    while (true){
        ram.updateStatus();

        cout.precision(3);
        cout << "RAM: " << ram.usingRamCapacity() << "Gb" << "/" << ram.totalRamCapacity() << "Gb" << endl;
        cout << "RAM(%): " << ram.usingRamInPercentage() << "%" << endl;
        Sleep(100);
        system("cls");
    }

    return 0;


    //MEMORYSTATUSEX statex;

    //statex.dwLength = sizeof(statex);
    //while(true) {
    //    GlobalMemoryStatusEx(&statex);

    //    float total_ram_capacity = static_cast<float>(statex.ullTotalPhys) / gb2b;
    //    float free_ram_capacity = static_cast<float>(statex.ullAvailPhys) / gb2b;
    //    float using_ram_capacity = total_ram_capacity - free_ram_capacity;
    //    int ram_percentage = statex.dwMemoryLoad;
    //    float total_pf_capacity = static_cast<float>(statex.ullTotalPageFile) / gb2b;
    //    float free_pf_capacity = static_cast<float>(statex.ullAvailPageFile) / gb2b;
    //    float using_pf_capacity = total_pf_capacity - free_pf_capacity;

    //    long start_timer = clock();

    //    cout.precision(3);    
    //    cout << "RAM: " << using_ram_capacity << "Gb" << "/" << total_ram_capacity << "Gb" << endl;
    //    cout << "RAM(%): " << ram_percentage << "%" << endl;
    //    cout << "CPU: " << GetCpuUsage() << "%" << endl; // ������ ���� % � �����������
    //    cout << "Page File: " << using_pf_capacity << "Gb" << "/" << total_pf_capacity << "Gb" << endl;

    //    Sleep(100);
    //    system("cls");      
    //}

    //

    //return 0;
}